# CISUA_Rate

[![CI Status](https://img.shields.io/travis/MaksAvksentev/CISUA_Rate.svg?style=flat)](https://travis-ci.org/MaksAvksentev/CISUA_Rate)
[![Version](https://img.shields.io/cocoapods/v/CISUA_Rate.svg?style=flat)](https://cocoapods.org/pods/CISUA_Rate)
[![License](https://img.shields.io/cocoapods/l/CISUA_Rate.svg?style=flat)](https://cocoapods.org/pods/CISUA_Rate)
[![Platform](https://img.shields.io/cocoapods/p/CISUA_Rate.svg?style=flat)](https://cocoapods.org/pods/CISUA_Rate)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CISUA_Rate is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CISUA_Rate'
```

## Author

MaksAvksentev, avksentiev5@icloud.com

## License

CISUA_Rate is available under the MIT license. See the LICENSE file for more info.
=======
