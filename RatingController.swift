//
//  RatingController.swift
//  CISUA_Rate
//
//  Created by Maksim Avksentev on 13.07.18.
//

import UIKit

// The protocol of the actions in RatingView.
@objc public protocol RatingDelegate {
    
    func didRate(rating rate: Double)
    func didIgnoreToRate()
    func didRateOnAppStore()
    func didIgnoreToRateOnAppStore()
    func didDissmissThankYouDialog()
}

public class RatingController: UIViewController {
    
    fileprivate let delegate = Rating.delegate
    
    fileprivate var rating: Double = 0.0 {
        didSet {
            self.rateButton.setTitleColor(rating > 0.0 ? defaultTintColor : UIColor.gray, for: .normal)
            self.rateButton.isEnabled = rating > 0.0
        }
    }
    
    fileprivate let defaultTintColor = UIColor(red:0.0, green:122.0/255.0, blue:1.0, alpha:1.0)
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var starView: StarView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    
    override public func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setupView()
        self.rating = 0.0
    }
    
    private func setupView() {
        
        self.containerView.layer.cornerRadius = 10
        
        self.setupStarRateView()
        
        self.titleLabel.text = Rating.titleLabelText
        self.descriptionLabel.text = Rating.descriptionLabelText
        
        self.cancelButton.setTitle(Rating.dismissButtonTitleText, for: .normal)
        self.cancelButton.setTitleColor(defaultTintColor, for: .normal)
        
        self.rateButton.setTitle(Rating.rateButtonTitleText, for: .normal)
        self.rateButton.setTitleColor(defaultTintColor, for: .normal)
    }
    
    private func setupStarRateView() {
        
        self.starView.delegate = self
        self.starView.fullImage = Rating.filledStar()
        self.starView.emptyImage = Rating.emptyStar()
    }
    
    // MARK: - Action
    @IBAction func cancelButtonTouched(_ sender: UIButton) {
        
        self.view.backgroundColor = UIColor.clear
        self.containerView.isHidden = true
        
        self.delegate?.didIgnoreToRate()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func rateButtonTouched(_ sender: UIButton) {
        
        self.view.backgroundColor = UIColor.clear
        self.containerView.isHidden = true
        
        let minRatingToAppStore = Rating.minRatingToAppStore > 5 ? 5 : Rating.minRatingToAppStore
        
        if rating >= minRatingToAppStore {
            
            showRateInAppStoreAlertController()
            //Only save last rated version if user rates more than mininum score
            UserDefaults.standard.set(Rating.appVersion, forKey: RatingUserDefaultsKey.lastVersionRatedKey.rawValue)
            
        } else {
            showThankyouAlertController()
        }
        
        delegate?.didRate(rating: rating)
    }
    
    func sendUserToAppStore() {
        
        guard let url = URL(string: "itms-apps://itunes.apple.com/us/app/itunes-u/id\(Rating.itunesId)?ls=1&mt=8&action=write-review") else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
        self.delegate?.didRateOnAppStore()
    }
    
    // MARK: Alerts
    func showThankyouAlertController() {
        
        guard Rating.shouldShowThankYouAlertController else {
            return
        }
        
        let disadvantageAlertController = UIAlertController(title: Rating.thankyouTitleLabelText, message: Rating.thankyouDescriptionLabelText, preferredStyle: .alert)
        
        disadvantageAlertController.addAction(UIAlertAction(title: Rating.thankyouDismissButtonTitleText, style: .default, handler: { (_) in
            self.delegate?.didDissmissThankYouDialog()
            self.dismiss(animated: false, completion: nil)
        }))
        
        self.present(disadvantageAlertController, animated: true, completion: nil)
    }
    
    func showRateInAppStoreAlertController() {
        
        let rateInAppStoreAlertController = UIAlertController(title: Rating.appStoreTitleLabelText, message: Rating.appStoreDescriptionLabelText, preferredStyle: .alert)
        
        rateInAppStoreAlertController.addAction(UIAlertAction(title: Rating.appStoreDismissButtonTitleText, style: .default, handler: { (_) in
            self.dismiss(animated: false, completion: nil)
            self.delegate?.didIgnoreToRateOnAppStore()
        }))
        
        rateInAppStoreAlertController.addAction(UIAlertAction(title: Rating.appStoreRateButtonTitleText, style: .default, handler: { (_) in
            self.sendUserToAppStore()
            self.dismiss(animated: false, completion: nil)
        }))
        
        self.present(rateInAppStoreAlertController, animated: true, completion: nil)
    }
    
}

extension RatingController: StarViewDelegate {
    
    public func starView(_ ratingView: StarView, didUpdate rating: Double) {
        self.rating = Double(String(format: "%.2f", rating)) ?? Double(rating)
    }
    
    public func starView(_ ratingView: StarView, isUpdating rating: Double) {
        self.rating = Double(String(format: "%.2f", rating)) ?? Double(rating)
    }
}
