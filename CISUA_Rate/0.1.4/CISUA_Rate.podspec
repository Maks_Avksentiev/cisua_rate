#pod lib lint CISUA_Rate.podspec'

Pod::Spec.new do |s|
  s.name             = 'CISUA_Rate'
  s.version          = '0.1.4'
  s.summary          = 'A short description of CISUA_Rate.'
  s.description      = <<-DESC
                        This library helps rate our applications in AppStore.
                       DESC

  s.requires_arc     = true
  s.homepage         = 'https://bitbucket.org/Maks_Avksentiev/cisua_rate/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Maks Avksentiev' => 'avksentiev5@icloud.com' }
  s.source           = { :git => 'https://Maks_Avksentiev@bitbucket.org/Maks_Avksentiev/cisua_rate.git', :branch => 'master', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.source_files = '**'
end
