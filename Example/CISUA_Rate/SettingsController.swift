//
//  SettingsController.swift
//  CISUA_Rate_Example
//
//  Created by Maksim Avksentev on 13.07.18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import CISUA_Rate

enum SettingType {
    case appVersion
    case daysUntilPrompt
    case daysRemindPeriod
    case minutesUntilPrompt
    case minitesRemindPeriod
    case minimumScore
}

class SettingsController: UITableViewController {
    
    var type: SettingType?
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.delegate = self
        
        guard let type = type else {
            return
        }
        
        switch type {
        case .appVersion:
            self.title = "App version"
            self.textField.text = "\(Rating.appVersion)"
            self.textField.keyboardType = .decimalPad
        case .daysUntilPrompt:
            self.title = "Day until prompt"
            self.textField.text = "\(Rating.daysUntilPrompt)"
        case .daysRemindPeriod:
            self.title = "Remind period"
            self.textField.text = "\(Rating.remindPeriod)"
        case .minutesUntilPrompt:
            self.title = "Minute until prompt"
            self.textField.text = "\(Rating.minuteUntilPrompt)"
        case .minitesRemindPeriod:
            self.title = "Minute remind period"
            self.textField.text = "\(Rating.minuteRemindPeriod)"
        case .minimumScore:
            self.title = "Minimum score"
            self.textField.text = "\(Rating.minRatingToAppStore)"
            self.textField.keyboardType = .decimalPad
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.textField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        guard let type = type, let value = textField.text else {
            return
        }
        
        switch type {
        case .appVersion:
            Rating.appVersion = value
        case .daysUntilPrompt:
            Rating.daysUntilPrompt = Int(value) ?? 0
        case .daysRemindPeriod:
            Rating.remindPeriod = Int(value) ?? 0
        case .minutesUntilPrompt:
            Rating.minuteUntilPrompt = Int(value) ?? 0
        case .minitesRemindPeriod:
            Rating.minuteRemindPeriod = Int(value) ?? 0
        case .minimumScore:
            Rating.minRatingToAppStore = Double(value) ?? 0
        }
    }
    
}

extension SettingsController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let type = type, type == .minimumScore {
            if let value = textField.text {
                if Double(value + string) ?? 0 > 5 {
                    return false
                }
            }
            return true
        }
        
        return true
    }
}
