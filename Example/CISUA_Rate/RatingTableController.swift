//
//  RatingTableController.swift
//  CISUA_Rate_Example
//
//  Created by Maksim Avksentev on 13.07.18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import CISUA_Rate

class RatingTableController: UITableViewController {

    @IBOutlet weak var debugModeSwitch: UISwitch!
    @IBOutlet weak var minuteUntilPromptLabel: UILabel!
    @IBOutlet weak var dayUntilPromptLabel: UILabel!
    @IBOutlet weak var minuteRemindPeriodLabel: UILabel!
    @IBOutlet weak var dayRemindPeriodLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var minimumScoreLabel: UILabel!
    @IBOutlet weak var currentRatedVersionLabel: UILabel!
    @IBOutlet weak var lastestRateVersionLabel: UILabel!
    @IBOutlet weak var firstDateUsingAppLabel: UILabel!
    @IBOutlet weak var latestRemindDateLabel: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Rating.delegate = self
        Rating.promptRateUsIfNeeded(in: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.setupView()
    }
    
    //MARK: - Private
    private func setupView() {
        
        self.debugModeSwitch.isOn = Rating.debugMode
        self.minuteUntilPromptLabel.text = "\(Rating.minuteUntilPrompt)"
        self.dayUntilPromptLabel.text = "\(Rating.daysUntilPrompt)"
        self.minuteRemindPeriodLabel.text = "\(Rating.minuteRemindPeriod)"
        self.dayRemindPeriodLabel.text = "\(Rating.remindPeriod)"
        self.appVersionLabel.text = "\(Rating.appVersion)"
        self.minimumScoreLabel.text = "\(Rating.minRatingToAppStore)"
        
        self.currentRatedVersionLabel.text = "\(Rating.currentAppVersion)"
        self.lastestRateVersionLabel.text = "\(Rating.lastVersionRated)"
        self.firstDateUsingAppLabel.text = "\(Rating.firstUsed)"
        self.latestRemindDateLabel.text = "\(Rating.lastRemind)"
        
        self.dayUntilPromptLabel.textColor = !Rating.debugMode ? UIColor.black : UIColor.gray
        self.dayRemindPeriodLabel.textColor = !Rating.debugMode ? UIColor.black : UIColor.gray
        self.minuteRemindPeriodLabel.textColor = Rating.debugMode ? UIColor.black : UIColor.gray
        self.minuteUntilPromptLabel.textColor = Rating.debugMode ? UIColor.black :UIColor.gray
        self.appVersionLabel.textColor = Rating.debugMode ? UIColor.black : UIColor.gray
    }
    
    // MARK: - Action
    @IBAction func rateNowButtonTouched(_ sender: UIButton) {
        
        Rating.promptRateUs(in: self)
    }
    
    @IBAction func rateWithConditionButtonTouched(_ sender: UIButton) {
        
        Rating.promptRateUsIfNeeded(in: self)
    }
    
    @IBAction func debugModeValueChanged(_ sender: UISwitch) {
        
        Rating.debugMode = sender.isOn
        self.setupView()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            switch indexPath.row {
            case 1:
                self.pushToEditVC(identifier: .minimumScore)
            case 2:
                if !Rating.debugMode {
                    self.pushToEditVC(identifier: .daysUntilPrompt)
                }
            case 3:
                if !Rating.debugMode {
                    self.pushToEditVC(identifier: .daysRemindPeriod)
                }
            case 4:
                if Rating.debugMode {
                    self.pushToEditVC(identifier: .minutesUntilPrompt)
                }
            case 5:
                if Rating.debugMode {
                    self.pushToEditVC(identifier: .minitesRemindPeriod)
                }
            case 6:
                if Rating.debugMode {
                    self.pushToEditVC(identifier: .appVersion)
                }
            default:
                break
            }
        }
    }
    
    func pushToEditVC(identifier: SettingType) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SettingTableViewController") as? SettingsController else {
            
            return
        }
        
        controller.type = identifier
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

extension RatingTableController: RatingDelegate {
    
    func didRate(rating: Double) {
        
        print("didRate: \(rating)")
        self.setupView()
    }
    
    func didRateOnAppStore() {
        
        print("didRateOnAppStore")
    }
    
    func didIgnoreToRate() {
        
        print("didIgnoreToRate")
    }
    
    func didIgnoreToRateOnAppStore() {
        
        print("didIgnoreToRateOnAppStore")
    }
    
    func didDissmissThankYouDialog() {
        
        print("didDissmissThankYouDialog")
    }
}
