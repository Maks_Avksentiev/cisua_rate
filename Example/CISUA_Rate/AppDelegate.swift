//
//  AppDelegate.swift
//  CISUA_Rate
//
//  Created by MaksAvksentev on 07/13/2018.
//  Copyright (c) 2018 MaksAvksentev. All rights reserved.
//

import UIKit
import CISUA_Rate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        self.configureRating()
        
        return true
    }
    
    //MARK: - Private
    private func configureRating() {
     
        Rating.itunesId = "1289293705"
        Rating.minRatingToAppStore = 3.5
        
        Rating.debugMode = true
        Rating.minuteUntilPrompt = 1
        Rating.minuteRemindPeriod = 1
        
//        You can configure your images with Rating
        //        Rating.filledStar =
        //        Rating.emptyStar =
    }
    
}

